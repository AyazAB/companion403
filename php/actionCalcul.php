<?php
  $id_type = strip_tags($_POST['id_type']);
  $id_calcul = strip_tags($_POST['id_calcul']);
  include "../db.php";
  $req = null;
  if ($id_calcul == 1) {
      $req = $db->prepare("SELECT AVG(temp) as resultat FROM item_temp WHERE id_type_item=?");
  } else {
      $req = $db->prepare("SELECT STD(temp) as resultat FROM item_temp WHERE id_type_item=?");
  }
  $values = array($id_type);
  $req->execute($values);
  foreach ($req as $row) {
      echo $row['resultat'];
  }
?>