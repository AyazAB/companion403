<?php
  $lieu = strip_tags($_POST['listeLieux']);
  $item = strip_tags($_POST['temp']);
  $type = strip_tags($_POST['listeTypeT']);
  $date = date('Y-m-d H:i:s');

  include "../db.php";
  $req = $db->prepare("INSERT INTO item_temp (id_lieu, temp, date_temp, id_type_item) VALUES (?,?,?,?)");

  $res = $req->execute(array(
      $lieu,
      $item,
      $date,
      $type,
  ));
  echo '<script type="text/javascript">alert("Votre donnée a bien été ajouté"); window.location.href = "../index.php";</script>';
  exit();
?>