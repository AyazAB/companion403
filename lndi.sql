-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Dim 09 Décembre 2018 à 12:08
-- Version du serveur :  5.7.24-0ubuntu0.18.04.1
-- Version de PHP :  7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `LNDI`
--

-- --------------------------------------------------------

--
-- Structure de la table `item_temp`
--

CREATE TABLE `item_temp` (
  `id` int(11) NOT NULL,
  `id_lieu` int(11) NOT NULL,
  `temp` float NOT NULL,
  `date_temp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_user` int(11) NOT NULL,
  `id_type_item` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `item_temp`
--

INSERT INTO `item_temp` (`id`, `id_lieu`, `temp`, `date_temp`, `id_user`, `id_type_item`) VALUES
(9, 2, 25, '2018-12-08 21:09:47', 0, 2),
(10, 1, 4, '2018-12-08 21:58:01', 0, 2),
(11, 1, 2, '2018-12-08 22:44:21', 0, 3),
(12, 1, 5, '2018-12-08 22:45:18', 0, 4),
(13, 1, 5, '2018-12-08 22:45:37', 0, 3),
(14, 1, 5, '2018-12-08 22:46:40', 0, 3),
(15, 1, 5, '2018-12-08 23:38:50', 0, 2);

-- --------------------------------------------------------

--
-- Structure de la table `lieu`
--

CREATE TABLE `lieu` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `lieu`
--

INSERT INTO `lieu` (`id`, `nom`) VALUES
(1, 'Sossusvlei (Sesriem)'),
(2, 'Cote des Squelettes'),
(3, 'Cape Cross');

-- --------------------------------------------------------

--
-- Structure de la table `type_item`
--

CREATE TABLE `type_item` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `id_type_unite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `type_item`
--

INSERT INTO `type_item` (`id`, `nom`, `id_type_unite`) VALUES
(2, 'Temperature Diurne', 1),
(3, 'Temperature Nocturne', 1),
(4, 'Hygrometrie', 2),
(5, 'Vitesse du vent', 3);

-- --------------------------------------------------------

--
-- Structure de la table `type_unite`
--

CREATE TABLE `type_unite` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `type_unite`
--

INSERT INTO `type_unite` (`id`, `nom`) VALUES
(1, '°C'),
(2, '%'),
(3, 'Km/h');

-- --------------------------------------------------------

--
-- Structure de la table `type_user`
--

CREATE TABLE `type_user` (
  `id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `id_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `mail`, `pass`, `id_type`) VALUES
(1, 'test', 'test@gmail.com', 'azerty1234', 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `item_temp`
--
ALTER TABLE `item_temp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_lieu` (`id_lieu`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `fk_foreign_id_type_item` (`id_type_item`);

--
-- Index pour la table `lieu`
--
ALTER TABLE `lieu`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `type_item`
--
ALTER TABLE `type_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_foreign_id_type_unite` (`id_type_unite`);

--
-- Index pour la table `type_unite`
--
ALTER TABLE `type_unite`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `type_user`
--
ALTER TABLE `type_user`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_type_2` (`id_type`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `item_temp`
--
ALTER TABLE `item_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `lieu`
--
ALTER TABLE `lieu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `type_item`
--
ALTER TABLE `type_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `type_unite`
--
ALTER TABLE `type_unite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `type_user`
--
ALTER TABLE `type_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `item_temp`
--
ALTER TABLE `item_temp`
  ADD CONSTRAINT `fk_foreign_id_type_item` FOREIGN KEY (`id_type_item`) REFERENCES `type_item` (`id`);

--
-- Contraintes pour la table `type_item`
--
ALTER TABLE `type_item`
  ADD CONSTRAINT `fk_foreign_id_type_unite` FOREIGN KEY (`id_type_unite`) REFERENCES `type_unite` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
