<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico" />
    <title>Companion 403</title>
    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body id="page-top">
<div id='filtre_color' style ='position:fixed;width:100%; height:100%; background-color:rgba(0,0,0,0.5);display:none; z-index:900;'></div>
<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
    <a class="navbar-brand mr-1" href="#"><img src="IconNuit.png" width=50px; height=50px;> Companion 403</a>
    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
    </button>
</nav>
<div id="wrapper">
    <!-- Account -->
    <div id="content-wrapper">
        <div class="container-fluid">
            <!-- Affichage Lieu -->
            <div id="saisieF" class='elements'>
                <div class="table-wrapper-scroll-y">
                    <table class="table table-bordered table-striped">
                        <?php
                        include ("db.php");
                        $req = "SELECT * FROM item_temp";
                        echo "<thead><tr><th scope='col'>Lieu</th><th scope='col'>Type</th><th scope='col'>Unité</th><th scope='col'>Valeur</th><th scope='col'>Date</th></tr></thead><tbody>";
                        foreach ($db->query($req) as $row){
                            echo "<tr>";
                            $req2 = "SELECT * from lieu WHERE id=".$row['id_lieu'];
                            foreach ($db->query($req2) as $row2){
                                echo "<th>".$row2['nom']."</th>";
                            }
                            $req3 = "SELECT * from type_item WHERE id=".$row['id_type_item'];
                            foreach ($db->query($req3) as $row3){
                                echo "<th>".$row3['nom']."</th>";
                                $req4 = "SELECT * from type_unite WHERE id=".$row3['id_type_unite'];
                                foreach ($db->query($req4) as $row4){
                                    echo "<th>".$row4['nom']."</th>";
                                }
                            }
                            echo "<th>".$row['temp']."</th>";
                            echo "<th>".$row['date_temp']."</th>";
                            echo "</tr>";
                        }
                        echo "</tbody>";
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
<!-- Sticky Footer -->
<footer class="sticky-footer-legacy">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright © 403 Forbidden</span>
        </div>
    </div>
</footer>
</div>
<!-- /.content-wrapper -->
</div>
<!-- /#wrapper -->
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Page level plugin JavaScript-->
<script src="vendor/chart.js/Chart.min.js"></script>
<script src="vendor/datatables/jquery.dataTables.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.js"></script>
<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>
<!-- Demo scripts for this page-->
<script src="js/demo/datatables-demo.js"></script>
<script src="js/demo/chart-area-demo.js"></script>
</body>
</html>