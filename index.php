

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="favicon.ico" />
      <title>Companion 403</title>
      <!-- Bootstrap core CSS-->
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Custom fonts for this template-->
      <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <!-- Page level plugin CSS-->
      <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="css/sb-admin.css" rel="stylesheet">
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   </head>
   <body id="page-top">
      <div id='filtre_color' style ='position:fixed;width:100%; height:100%; background-color:rgba(0,0,0,0.5);display:none; z-index:900;'>
      </div>
      <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
         <a class="navbar-brand mr-1" href="index.php"><img src="IconNuit.png" width=50px; height=50px;> Companion 403</a>
         <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
         <i class="fas fa-bars"></i>
         </button>
         <!-- Navbar Search -->
         <!-- Navbar -->
      </nav>
      <div id="wrapper">
         <!-- Sidebar -->
         <ul class="sidebar navbar-nav">
            <li class="nav-item active">
               <a id="nav-stats" class="nav-link" href="#">
               <i class="fas fa-fw fa-tachometer-alt"></i>
               <span>Tableau de bord</span>
               </a>
            </li>
            <li class="nav-item active">
               <a id="nav-place" class="nav-link" href="#">
               <i class="fas fa-map-marked-alt"></i>
               <span>Ajouter lieu</span>
               </a>
            </li>
            <li class="nav-item active">
               <a id="nav-temperature" class="nav-link" href="#">
               <i class="fas fa-plus-circle"></i>
               <span>Ajouter donnée</span>
               </a>
            </li>
            <li class="nav-item active">
               <a id="nav-table" class="nav-link" href="#">
               <i class="fas fa-table"></i>
               <span>Voir données</span>
               </a>
            </li>
            <li class="nav-item active">
               <a id="nav-calcul" class="nav-link" href="#">
               <i class="fas fa-calculator"></i>
               <span>Calcul des données</span>
               </a>
            </li>
             <li class="nav-item active">
                 <a id="nav-code" class="nav-link" href="#">
                     <i class="fas fa-calculator"></i>
                     <span>[TEST] QR Code</span>
                 </a>
             </li>
            </li>   
         </ul>
         <!-- End Sidebar -->
         <!-- Account -->
         <div class="container" id="connect" style='display:none; position:absolute; z-index:1000'>
            <!-- SignIn -->
            <div class="card card-login mx-auto mt-5" id='form_connexion'>
               <div class="card-header">Se connecter</div>
               <div class="card-body">
                  <form>
                     <div class="form-group">
                        <div class="form-label-group">
                           <input type="email" id="inputEmail" class="form-control" placeholder="thierry-breton@atos.com" required="required" autofocus="autofocus">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="form-label-group">
                           <input type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" required="required">
                           <label for="inputPassword">Mot de passe</label>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="checkbox">
                           <label>
                           <input type="checkbox" value="remember-me">
                           Enregistrer mot de passe
                           </label>
                        </div>
                     </div>
                     <a class="btn btn-primary btn-block" href="index.php">Se connecter</a>
                  </form>
                  <div class="text-center">
                     <a class="d-block small mt-3" id='sign_in' href="#">S'inscrire</a>
                  </div>
               </div>
            </div>
            <!-- End SignIn -->
            <!-- SignUp -->
            <div class="card card-login mx-auto mt-5" id='form_inscription' style="display:none">
               <div class="card-header">S'inscrire</div>
               <div class="card-body">
                  <form>
                     <div class="form-group">
                        <div class="form-label-group">
                           <input type="email" id="inputEmail" class="form-control" placeholder="thierry-breton@atos.com" required="required" autofocus="autofocus">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="form-label-group">
                           <input type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" required="required">
                           <label for="inputPassword">Mot de passe</label>
                        </div>
                     </div>
                     <a class="btn btn-primary btn-block" href="index.php">S'inscrire</a>
                  </form>
               </div>
            </div>
            <!-- End SignUp -->
            <!-- End Account -->
         </div>
         <div id="content-wrapper">
            <div class="container-fluid">
               <div id="stats" class='elements' style="display:none">
                  <div class="card mb-3">
                     <div class="card-header">
                        <i class="fas fa-chart-area"></i>
                        Statistiques
                     </div>
                     <div class="card-body">
                        <canvas id="myAreaChart" width="100%" height="30"></canvas>
                     </div>
                     <div class="card-footer small text-muted"></div>
                  </div>
               </div>
               <!-- Ajout Lieu -->
               <div id="saisieL" class='elements' style="display:none">
                  <div class="card text-white bg-info mb-3">
                     <div class="card-header"><i class="fas fa-info-circle"></i> Aide</div>
                     <div class="card-body">
                        Ici vous pouvez ajouter un nouveau Lieu.<br>
                        Remplissez le champ de la section "Lieu" puis cliquez sur  le bouton "Valider".
                     </div>
                  </div>
                  <form method="POST" action="./php/actionLieu.php">
                     <h3>Lieu</h3>
                     <input id="searchLieu" name="lieutarace" type="text" class="form-control" placeholder="Exemple: Kandy"/> <br />
                     <button type="submit" class="btn btn-success">Valider</button>
                  </form>
               </div>
               <!-- Fin Ajout Lieu -->
               <!-- Ajout donnée -->
               <div id="saisieT" class='elements' style="display:none">
                  <div class="card text-white bg-info mb-3">
                     <div class="card-header"><i class="fas fa-info-circle"></i> Aide</div>
                     <div class="card-body">
                        Ici vous pouvez ajouter votre donnée.<br>
                        Choisissez le lieu où vous l'avez recueillie, puis dans la section Donnée sélectionnez le type de donnée à ajouter et enfin entrez votre donnée dans le champ "Valeur".
                     </div>
                  </div>
                  <form  method="POST" action="./php/actionTemp.php">
                  <h3>Lieu</h3>
                  <select name="listeLieux" class="custom-select">
                  <?php
                     include ("db.php");
                     $req = "SELECT * FROM lieu";
                     foreach ($db->query($req) as $row){
                       echo "<option value=".$row['id'].">".$row['nom']."</option>";
                     }
                     ?>
                  </select>
                  <br>
                  <br>
                  <h3>Donnée</h3>
                  <div class="form-row">
                     <div class="form-group col-md-4">
                        <h5>Type</h5>
                        <select name="listeTypeT" class="custom-select">
                        <?php
                           include ("db.php");
                           $req = "SELECT * FROM type_item";
                           foreach ($db->query($req) as $row){
                             $req2 = "SELECT * FROM type_unite WHERE id=".$row['id_type_unite'];
                             foreach ($db-> query($req2) as $row2){
                               echo "<option value=".$row['id'].">".$row['nom']." (".$row2['nom'].")</option>";
                             }
                           }
                        ?>
                        </select>
                     </div>
                     <div class="form-group col-md-4">
                        <h5>Valeur</h5>
                        <input type="number" name="temp" class="form-control"/>
                     </div>
                  </div>
                  <br />
                  <button type="submit" class="btn btn-success btn-lg">Valider</button>
                  <form>
               </div>
               <!-- Fin Ajout Température -->
               <!-- Affichage Lieu -->
               <div id="saisieF" class='elements' style="display:none">
                  <div class="card text-white bg-info mb-3">
                     <div class="card-header"><i class="fas fa-info-circle"></i> Aide</div>
                     <div class="card-body">
                        Ici vous pouvez voir la totalité de vos données.<br>
                        Les données sont présentés sous forme d'un tableau séparé en 5 colonnes: Lieu, Type, Unité, Valeur et Date.<br>
                        Vous pouvez supprimer une donnée en cliquant sur l'icon de la poubelle.
                     </div>
                  </div>
                  <div class="table-wrapper-scroll-y">
                     <table class="table table-bordered table-striped">
                        <?php
                           include ("db.php");
                           $req = "SELECT * FROM item_temp";
                           echo "<thead><tr><th scope='col'>Lieu</th><th scope='col'>Type</th><th scope='col'>Unité</th><th scope='col'>Valeur</th><th scope='col'>Date</th><th scope='col'>Action</th></tr></thead><tbody>";
                           foreach ($db->query($req) as $row){
                             echo "<tr>";
                             $req2 = "SELECT * from lieu WHERE id=".$row['id_lieu'];
                             foreach ($db->query($req2) as $row2){
                               echo "<th>".$row2['nom']."</th>";
                             }
                             $req3 = "SELECT * from type_item WHERE id=".$row['id_type_item'];
                             foreach ($db->query($req3) as $row3){
                               echo "<th>".$row3['nom']."</th>";
                               $req4 = "SELECT * from type_unite WHERE id=".$row3['id_type_unite'];
                               foreach ($db->query($req4) as $row4){
                                 echo "<th>".$row4['nom']."</th>";
                               }
                             }
                             echo "<th>".$row['temp']."</th>";
                             echo "<th>".$row['date_temp']."</th>";
                             echo "<th><i name=".$row['id']." class='fas fa-trash'></i></th>";
                             echo "</tr>";
                           }
                           echo "</tbody>";
                           ?>
                     </table>
                  </div>
               </div>
               <!-- Fin affichage Lieu -->
               <!-- Calcul -->
               <div id="calcul" class='elements' style="display:none">
                  <div class="card text-white bg-info mb-3">
                     <div class="card-header"><i class="fas fa-info-circle"></i> Aide</div>
                     <div class="card-body">
                        Ici vous pouvez faire des calculs sur vos données.<br>
                        Choisissez le type de calcul souhaité dans la section "Calcul" puis sélectionnez le type de donnée sur lequel vous voulez faire ce calcul.<br>
                        Pour finir cliquez sur le bouton "Valider".
                     </div>
                  </div>
                  <form  method="POST" action="">
                  <h5>Calcul</h5>
                  <select id ="listeCalcul" name="listeCalcul" class="custom-select">
                     <option value="1">Moyenne</option>
                     <option value="2">Écart-type</option>
                  </select>
                  <br>
                  <br>
                  <h5>Type de donnée</h5>
                  <select id="listeType" name="listeType" class="custom-select">
                  <?php
                     include ("db.php");
                     $req = "SELECT * FROM type_item";
                     foreach ($db->query($req) as $row){
                       $req2 = "SELECT * FROM type_unite WHERE id=".$row['id_type_unite'];
                       foreach ($db-> query($req2) as $row2){
                         echo "<option value=".$row['id'].">".$row['nom']." (".$row2['nom'].")</option>";
                       }
                     }
                     ?>
                  </select>
                  <br>
                  <br>
                  <button type="button" class="btn btn-success btn-lg" id="adel">Valider</button>
                  <br><br><br>        
                  <div class="card bg-light">
                     <div class="card-header">Résultat</div>
                     <div id="result" class="card-body">  
                     </div>
                  </div>
                  <form>
               </div>
               <!-- Fin Calcul -->
                <!-- [TEST] QR Code -->
                <div id="qr" class='elements' style="display:none">
                    <img src="qrcode.jpeg">
                </div>
                <!-- Fin [TEST] QR Code -->
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
      <!-- Sticky Footer -->
      <footer class="sticky-footer">
         <div class="container my-auto">
            <div class="copyright text-center my-auto">
               <span>Copyright © 403 Forbidden</span>
            </div>
         </div>
      </footer>
      </div>
      <!-- /.content-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
      </a>
      <!-- Logout Modal-->
      <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Prêt(e) à partir?</h5>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
               <div class="modal-body">Selectionnez "Déconnexion" pour vous déconnecter</div>
               <div class="modal-footer">
                  <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>
                  <a class="btn btn-primary" href="login.html">Déconnexion</a>
               </div>
            </div>
         </div>
      </div>
      <!-- Bootstrap core JavaScript-->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- Core plugin JavaScript-->
      <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
      <!-- Page level plugin JavaScript-->
      <script src="vendor/chart.js/Chart.min.js"></script>
      <script src="vendor/datatables/jquery.dataTables.js"></script>
      <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="js/sb-admin.min.js"></script>
      <!-- Demo scripts for this page-->
      <script src="js/demo/datatables-demo.js"></script>
      <script src="js/demo/chart-area-demo.js"></script>
   </body>
</html>

